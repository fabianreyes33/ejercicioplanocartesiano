/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class PlanoCartesiano {
    
    // (5,7)...(-9,6)
    
    private String urlPuntos;
    private Funcion funciones[];

    public PlanoCartesiano() {
    }

    
    public PlanoCartesiano(String urlPuntos) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv");
        Object datos[]=archivo.leerArchivo();
        //No se toma en cuenta la primera línea del archivo , ya que está contiene el formato de los datos
        //Crear los espacios para las funciones:
        this.funciones=new Funcion[datos.length-1];
    }

    
    
    
    public Funcion[] getFunciones() {
        return funciones;
    }

    public void setFunciones(Funcion[] funciones) {
        this.funciones = funciones;
    }
    
    
}
